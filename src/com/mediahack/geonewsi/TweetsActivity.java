/**
 *         2014  Guillermo Robles

    This file is part of Geonewsies.

    Geonewsies is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Geonewsies is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Geonewsies.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mediahack.geonewsi;

import java.util.ArrayList;

import com.mediahack.geonewsi.social_media.DownloaderTask_Media;
import com.mediahack.geonewsi.social_media.DownloaderTask_Tweet;
import com.mediahack.geonewsi.social_media.Tweet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * 
 * @author guiller
   TweetsActivity.java
   2014
 *
 */
public class TweetsActivity extends Activity {

	public static String KEY_TRENDING = "key_trending"; 
	
	private ListView listView;
	private Context context;
	private ProgressDialog progress;
	private TweetsAdapter tAdapter; 
	
	public void onCreate(Bundle savedInstanceState) {    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main);
		progress = new ProgressDialog(this);
		  loading(true);
		
		listView= (ListView) findViewById(R.id.listNews);
		tAdapter = new TweetsAdapter(this.getBaseContext());
		listView.setAdapter(tAdapter);
		
		//DataModel
		DownloaderTask_Tweet task = new DownloaderTask_Tweet(this);
		
		task.execute(getIntent().getStringExtra(KEY_TRENDING));
		
		 	getActionBar().setDisplayHomeAsUpEnabled(true);
	        //getActionBar().setHomeButtonEnabled(true);
		
	
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    
	    case android.R.id.home: // Respond to the action bar's Up/Home button
	      /* Intent intent = NavUtils.getParentActivityIntent(this);
	       intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	       NavUtils.navigateUpTo(this, intent);
	       Utils.closeTransition(this);*/
	    	finish(); //TODO it is wrong, bad design 
	        return true;
	    }
	
	    return super.onOptionsItemSelected(item);
	}
	
	public void updateTweets(ArrayList<Tweet> result){
		int max = result.size();
		for(int i = 0; i<max; i++)
			tAdapter.add(result.get(i));
		
			loading(false);
	}
	
	private void loading(boolean state){
		if (state == true) {
			progress.setMessage("Downloading");
			progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progress.setIndeterminate(true);
			progress.show();
		}
		if (state == false)
			progress.cancel();
	}
}
