/**
 *         2014  Guillermo Robles

    This file is part of Geonewsies.

    Geonewsies is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Geonewsies is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Geonewsies.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mediahack.geonewsi.news;

import java.util.ArrayList;

import com.mediahack.geonewsi.MainActivity.PlaceholderFragment;

import android.os.AsyncTask;

/**
 * 
 * @author guiller
   DownloaderTask.java
   2014
 *
 */
public class DownloaderTask extends AsyncTask<String, Void, ArrayList<News>> {

	private PlaceholderFragment fragment;
	
	public DownloaderTask(PlaceholderFragment fragment){
		this.fragment = fragment;
	}
	
	@Override
	protected ArrayList<News> doInBackground(String... params) {
		//Execute Guardian API
	
		Guardian g = new Guardian(params[0]);
		return g.listNews();
	}
	
	@Override
	public void onPostExecute(ArrayList<News> result) {
		super.onPostExecute(result);

		if (fragment != null) {
			fragment.updateNews(result);
		}

	}

}
