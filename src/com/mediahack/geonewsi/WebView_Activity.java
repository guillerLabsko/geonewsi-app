/**
 *         2014  Guillermo Robles

    This file is part of Geonewsies.

    Geonewsies is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Geonewsies is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Geonewsies.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mediahack.geonewsi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * 
 * @author guiller
   WebView_Activity.java
   2014
 *
 */
public class WebView_Activity extends Activity {

	public static String URL_ID =  "urlextra";
	private ProgressDialog progress;
	private Activity activity;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_webview);
		WebView webview = (WebView) findViewById(R.id.webView);

		activity = this;
		
	 String URL =  this.getIntent().getExtras().getString(URL_ID);

	        webview.clearCache(true);
	        webview.clearHistory();
	        WebSettings webSettings = webview.getSettings();
	        webSettings.setJavaScriptEnabled(true);
	        webSettings.setDomStorageEnabled(true);
	        webview.setWebViewClient(new WebViewClient(){

	            @Override
	            public void onLoadResource (WebView view, String url) {
	           
					//show progress bar
	                if (progress == null) {
	                    progress = new ProgressDialog(activity);
	                    progress.setMessage("Loading please wait...");
	                    progress.show();
	                }   
	            }

	            @Override
	            public void onPageFinished(WebView view, String url) {
	                if (progress.isShowing()) {
	                    progress.dismiss();
	                }
	            }

	        });
	        //webview.setWebChromeClient(new WebChromeClient());
	       // webview.addJavascriptInterface(new JavaScriptInterface(this), "Android");

	        setContentView(webview);
	       webview.loadUrl(URL);
	   
	   
	      /* getActionBar().setHomeButtonEnabled(true); // diferences?¿ */
	        getActionBar().setDisplayHomeAsUpEnabled(true);
	        //getActionBar().setHomeButtonEnabled(true);
	
	
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    
	    case android.R.id.home: // Respond to the action bar's Up/Home button
	      /* Intent intent = NavUtils.getParentActivityIntent(this);
	       intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	       NavUtils.navigateUpTo(this, intent);
	       Utils.closeTransition(this);*/
	    	finish(); //TODO it is wrong, bad design 
	        return true;
	    }
	
	    return super.onOptionsItemSelected(item);
	    }
	
	
}
