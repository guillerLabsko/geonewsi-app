/**
 *         2014  Guillermo Robles

    This file is part of Geonewsies.

    Geonewsies is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Geonewsies is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Geonewsies.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mediahack.geonewsi.social_media;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * @author guiller
   YahooGeoPlace.java
   2014
 *
 */
public class YahooGeoPlace {
//http://query.yahooapis.com/v1/public/yql?q=select*from%20geo.places%20where%20text=%22spain%22&format=json
	
	
	private String basePoint = "http://query.yahooapis.com/v1/public/yql?q=select*from%20geo.places%20where%20text=%22";
	private String formatPoint = "%22&format=json";
	private String country= "";
	private int WOEID = 1; 
	
	public YahooGeoPlace(String country){
		this.country = country;
		initiate();
	}
	
	
	private String endPoint(String country){
		String tmp = country.replace(" ", "%20");
		return basePoint+tmp+formatPoint;
	}
	
	/*public static int getWOEID(String country){
	
		//woeid
		switch(country){
		case "UK":
			return 23424975;
		case "france":
			return 23424819;
		case "germany":
			return 23424829;
		case "denmark":
			return 23424796;
		case "Spain":
			return 23424950;
			
		}
		
		return 1; //Defect World
		
	}*/
	
	private String getResponse() throws IOException{
		URL url = new URL(this.endPoint(country));
		BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
		String entrada;
		String cadena="";
		while ((entrada = br.readLine()) != null){
			cadena = cadena + entrada;
		}
		return cadena;
	}

	
	private void initiate(){
		//ArrayList<News> listNews = new ArrayList<News>();
		try {
			String response = getResponse();
			
			if(response!=null){

			JSONObject oJson = new JSONObject(response);
			

			//Status OK
			if(response!=null){
				
				JSONObject oJsonQuery = oJson.getJSONObject("query");
				
				int count = oJsonQuery.getInt("count");
				
				if(count>1){
				
				JSONObject oJsonResults = oJsonQuery.getJSONObject("results");
				
				JSONArray array_place = oJsonResults.getJSONArray("place");
				
				JSONObject json = array_place.getJSONObject(0);
				WOEID = json.getInt("woeid");
				}
				if(count == 1){
					JSONObject oJsonResults = oJsonQuery.getJSONObject("results");
					
					JSONObject oJsonplace = oJsonResults.getJSONObject("place");
					
					WOEID = oJsonplace.getInt("woeid");
				}
			}
			
			}
		}
				
			
		 catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		
	}
	
	public int getWOEID(){
		return WOEID;
	}
	
}
