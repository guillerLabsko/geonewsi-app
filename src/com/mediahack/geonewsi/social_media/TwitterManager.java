/**
 *         2014  Guillermo Robles

    This file is part of Geonewsies.

    Geonewsies is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Geonewsies is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Geonewsies.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mediahack.geonewsi.social_media;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Trends;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
/**
 * 
 * @author guiller
   TwitterManager.java
   2014
 *
 */
public class TwitterManager {
	
	
	private Twitter twitter;
	
	private String ACCESS_TOKEN = "2424378884-zTZGO8BFosgswLY4FfcCj4xsa6VSn5VSnDWCGgV";
	private String ACCESS_TOKEN_SECRET = "QEY8fahJBuVODGMT3qvC0HB9HzQRcKe70FuM1mMu3qNgT";
	private String KEY = "W8rWnji5Hs0cSxaBAUnc3BDmw";
	private String SECRET = "K9ponFICMxlM379Mpc7ncuul4epMoVAOWMvE2O9G2FN0dNAVev";
	
	public TwitterManager(){
		//finalEndPoint = jSTrend_Place+woeid_London;
		twitter = this.getTwitter();

	
		
	}
	
	public ArrayList<Tweet> listTweets(int woeid){
		
	
		ArrayList<Tweet> listTemp = new ArrayList<Tweet>();
		
		
		
		Trends trends;
		try {
			trends = twitter.getPlaceTrends(woeid);
			for (int i = 0; i < trends.getTrends().length; i++) {
				String tmp = trends.getTrends()[i].getName();
				Tweet temp = new Tweet(tmp,tmp);
				listTemp.add(temp);
			}
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		
		return listTemp;
	}
	
	public ArrayList<Tweet> listTweetsbyTrending(String trending){
		ArrayList<Tweet> listTemp = new ArrayList<Tweet>();
		
		Query q = new Query();
		q.setQuery(trending);
		q.setResultType(q.MIXED); // MIXED between popular and recent
		
		try {
			QueryResult qr = twitter.search(q);
			int max = qr.getCount();
			for(int i = 0; i<max; i++ ){
				String author = qr.getTweets().get(i).getUser().getName();
				String ms = qr.getTweets().get(i).getText();
				Tweet temp = new Tweet(author,ms);
				listTemp.add(temp);
			}
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return listTemp;
	}
	
	// Authorization needed to get data from  Twitter...
	 @SuppressLint("NewApi")
	private Twitter getTwitter() {
		 
	        ConfigurationBuilder cb = new ConfigurationBuilder();
	        cb.setOAuthConsumerKey(KEY);
	        cb.setOAuthConsumerSecret(SECRET);
	        cb.setOAuthAccessToken(ACCESS_TOKEN);
	        cb.setOAuthAccessTokenSecret(ACCESS_TOKEN_SECRET);
	        return new TwitterFactory(cb.build()).getInstance();
	   
	 }

}
