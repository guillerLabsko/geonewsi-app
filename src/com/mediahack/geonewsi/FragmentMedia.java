/**
 *         2014  Guillermo Robles

    This file is part of Geonewsies.

    Geonewsies is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Geonewsies is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Geonewsies.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mediahack.geonewsi;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.mediahack.geonewsi.news.News;
import com.mediahack.geonewsi.social_media.DownloaderTask_Media;
import com.mediahack.geonewsi.social_media.Tweet;
/**
 * 
 * @author guiller
FragmentMedia.java
2014

 *
 */
	public class FragmentMedia extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";
		
		public static String key_preferences = "first_category";
		public static String key_location = "list_location";

		private SocialMediaAdapter mediaAdapter;
		private ListView listView;
		private Context context;
		private ProgressDialog progress;
		
		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static FragmentMedia newInstance(int sectionNumber) {
			FragmentMedia fragment = new FragmentMedia();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public FragmentMedia() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			context = this.getActivity().getBaseContext();
			progress = new ProgressDialog(this.getActivity());
			  loading(true);
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			listView= (ListView) rootView.findViewById(R.id.listNews);
			mediaAdapter = new SocialMediaAdapter(this.getActivity());
			listView.setAdapter(mediaAdapter);
			
			// Take preferences
			SharedPreferences sharedpreferences;
			sharedpreferences = PreferenceManager.getDefaultSharedPreferences(this.getActivity());

			String key_country  = sharedpreferences.getString(key_location, "uk");
			
			
			//DataModel
			DownloaderTask_Media task = new DownloaderTask_Media(this);
			task.execute(key_country);
			
			//Onclick item of the list view
			onClickItem();
			
			return rootView;
		
		
		}

		private void loading(boolean state){
			if (state == true) {
				progress.setMessage("Downloading");
				progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progress.setIndeterminate(true);
				progress.show();
			}
			if (state == false)
				progress.cancel();
		}
		
		public void updateTweets(ArrayList<Tweet> result){
			int max = result.size();
			for(int i = 0; i<max; i++)
				mediaAdapter.add(result.get(i));
			
				loading(false);
		}
		
		private void onClickItem(){	
			listView.setOnItemClickListener(new OnItemClickListener() {
	 
	            @Override
				public void onItemClick(AdapterView<?> parent, View view, int position,
	                    long id) {
	                Intent intent = new Intent(context, TweetsActivity.class);
	               
	                Tweet tweet = (Tweet)mediaAdapter.getItem(position);
	                
	                intent.putExtra(TweetsActivity.KEY_TRENDING, tweet.message);
	                startActivity(intent);
	            }
	        });
		}
		
		
	
}
