/**
 *         2014  Guillermo Robles

    This file is part of Geonewsies.

    Geonewsies is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Geonewsies is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Geonewsies.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mediahack.geonewsi;

import java.util.ArrayList;
import java.util.Locale;

import com.mediahack.geonewsi.news.DownloaderTask;
import com.mediahack.geonewsi.news.News;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentPagerAdapter;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
/**
 * 
 * @author guiller
MainActivity.java
2014

 *
 */
public class MainActivity extends ActionBarActivity implements
		ActionBar.TabListener {
	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a {@link FragmentPagerAdapter}
	 * derivative, which will keep every loaded fragment in memory. If this
	 * becomes too memory intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	
	
	public static String key_preferences = "first_category";
	public static String key_location = "list_location";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Set up the action bar.
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						
						actionBar.setSelectedNavigationItem(position); 
					}
				});
		
		//Adding Tab 
		actionBar.addTab(actionBar.newTab()
				.setText("News")
				.setTabListener(this));
		actionBar.addTab(actionBar.newTab()
				.setText("Social Media")
				.setTabListener(this));		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here.
		int id = item.getItemId(); 
		if (id == R.id.action_settings) {
			startActivity(new Intent(this, Preferences.class));
			return true;
		}
		
		if (id == R.id.action_refresh) { //TODO
			this.recreate();
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}
	


	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		Fragment array_fragments[] = {PlaceholderFragment.newInstance(0), FragmentMedia.newInstance(1)};
		
		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
			
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a PlaceholderFragment (defined as a static inner class
			// below).
			
		 return array_fragments[position]; //TODO define which fragment should be opened
		}

		@Override
		public int getCount() {
			// Show 2 total pages.
			return 2;
		}
		

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);

			}
			return null;
		}
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		private NewsAdapter newsAdapter;
		private DownloaderTask task;
		private ListView listView;
		private Context context;
		private ProgressDialog progress;
		
		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PlaceholderFragment newInstance(int sectionNumber) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			
			context = this.getActivity().getApplicationContext();
			progress = new ProgressDialog(this.getActivity());
			  loading(true);
			
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			listView = (ListView) rootView.findViewById(R.id.listNews);
			newsAdapter = new NewsAdapter(this.getActivity());
			listView.setAdapter(newsAdapter);
			
			//Take Preferences
			SharedPreferences sharedpreferences;
			sharedpreferences = PreferenceManager.getDefaultSharedPreferences(this.getActivity());

			String key_country  = sharedpreferences.getString(key_location, "uk");
			Log.i("KEY_COUNTRY", key_country);
			
			//DataModel
			task = new DownloaderTask(this);
			task.execute(key_country);
			
			//Onclick item of the list view
			onClickItem();
			
			return rootView;
			
		}
		
		
		private void onClickItem(){	
			listView.setOnItemClickListener(new OnItemClickListener() {
	 
	            @Override
				public void onItemClick(AdapterView<?> parent, View view, int position,
	                    long id) {
	                Intent intent = new Intent(context, WebView_Activity.class);
	                News news = (News)newsAdapter.getItem(position);
	                intent.putExtra(WebView_Activity.URL_ID, news.getUrl());
	                startActivity(intent);
	            }
	        });
		}
		
		private void loading(boolean state){
			if (state == true) {
				progress.setMessage("Downloading");
				progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progress.setIndeterminate(true);
				progress.show();
			}
			if (state == false)
				progress.cancel();
		}
		
	
		
		
		public void updateNews(ArrayList<News> result){		
			int max = result.size();
			for(int i = 0; i<max; i++)
				newsAdapter.add(result.get(i));
					
			
			loading( false);
		}
		
		
		

	}

}
