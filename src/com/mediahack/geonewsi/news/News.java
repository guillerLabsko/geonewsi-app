/**
 *         2014  Guillermo Robles

    This file is part of Geonewsies.

    Geonewsies is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Geonewsies is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Geonewsies.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mediahack.geonewsi.news;

/**
 * 
 * @author guiller
   News.java
   2014
 *
 */
public class News {

	//<ID>
	public static String id_ID = "id";
	public static String id_sectionID = "sectionId";
	public static String id_sectionName = "sectionName";
	public static String id_webPublicationDate ="webPublicationDate";
	public static String id_webTitle = "webTitle";
	public static String id_webUrl = "webUrl";
	public static String id_apiUrl = "apiUrl";
	//</ID>
	
	private String url;
	private String sectionName;
	private String title;
	
	public News(){
		
	}
	
	public News(String url, String sectionName, String title){
		this.url = url;
		this.sectionName = sectionName;
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
