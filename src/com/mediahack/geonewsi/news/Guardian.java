/**
 *         2014  Guillermo Robles

    This file is part of Geonewsies.

    Geonewsies is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Geonewsies is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Geonewsies.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mediahack.geonewsi.news;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * @author guiller
   Guardian.java
   2014
 *
 */
public class Guardian {
	
	public static String LOG = "GuardianService";
	
	public static String id_response = "response";
	public static String id_section ="section";
	public static String id_result ="results";
	
	
	
	private String finalPoint;
	private Response response;

	private String pointSearch = "http://content.guardianapis.com/world/"; //&api-key=mediahackdays2014"
	
	public Guardian(){
		finalPoint = "http://content.guardianapis.com/uk-news?show-fields=productionOffice&show-editors-picks=true&show-most-viewed=true&date-id=date%2Flast24hours&api-key=mediahackdays2014";
		response = new Response();

	}
	public Guardian(String country){
		//finalPoint = //http://explorer.content.guardianapis.com/#/search?q=spain&api-key=mediahackdays2014
		
		finalPoint = customizeEndPoint(country);
		
		response = new Response();
	}
	
	private String getResponse() throws IOException{
		URL url = new URL(finalPoint);
		BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
		String entrada;
		String cadena="";
		while ((entrada = br.readLine()) != null){
			cadena = cadena + entrada;
		}
		return cadena;
	}
	
	public ArrayList<News> listNews(){
		return initiate();
	}
	
	private ArrayList<News> initiate(){
		ArrayList<News> listNews = new ArrayList<News>();
		try {
			String response = getResponse();
			
			if(response!=null){

			JSONObject oJson = new JSONObject(response);
			

			//Status OK
			if(oJson.getJSONObject(id_response).getString(Response.id_status).equalsIgnoreCase("ok")){
				
				JSONObject oJsonResponse = oJson.getJSONObject(id_response);
				
				JSONArray news_array = oJsonResponse.getJSONArray(id_result);
				
				int size = news_array.length();
				for(int i = 0; i<size; i++){
					JSONObject jsonNews = news_array.getJSONObject(i);
					News news = new News(jsonNews.getString(News.id_webUrl),jsonNews.getString(News.id_sectionName),jsonNews.getString(News.id_webTitle));
					listNews.add(news);
				}
			} 

			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return listNews;
	}
	
	private String customizeEndPoint(String country){
		switch(country){
		case "uk":
			return "http://content.guardianapis.com/uk-news?show-fields=productionOffice&show-editors-picks=true&show-most-viewed=true&date-id=date%2Flast24hours";
		
		
		}
		String tmp = country.toLowerCase();
		return this.pointSearch+tmp;
	}
}
