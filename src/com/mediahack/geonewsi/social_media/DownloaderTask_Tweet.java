/**
 *         2014  Guillermo Robles

    This file is part of Geonewsies.

    Geonewsies is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Geonewsies is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Geonewsies.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mediahack.geonewsi.social_media;

import java.util.ArrayList;

import android.os.AsyncTask;

import com.mediahack.geonewsi.FragmentMedia;
import com.mediahack.geonewsi.TweetsActivity;

/**
 * 
 * @author guiller
   DownloaderTask_Tweet.java
   2014
 *
 */
public class DownloaderTask_Tweet extends AsyncTask<String, Void, ArrayList<Tweet>>{


		private TweetsActivity activity;
		
		public DownloaderTask_Tweet(TweetsActivity activity) {
			this.activity = activity;
		}
		
		@Override
		protected ArrayList<Tweet> doInBackground(String... params) {
			TwitterManager mTwitter = new TwitterManager();
			return mTwitter.listTweetsbyTrending(params[0]);
		}
		
		@Override
		public void onPostExecute(ArrayList<Tweet> result) {
			super.onPostExecute(result);

			if (activity != null) {
				activity.updateTweets(result);
			}

		}
}
